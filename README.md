# aws-infra

setup gitlab-ci vars
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION



0. Prerequisites
- Install terraform binary
- AWS Authentication
copy keys into .env
AWS_ACCESS_KEY_ID=******
AWS_DEFAULT_REGION=us-east-1
AWS_SECRET_ACCESS_KEY=******
TFSTATE_BUCKET=********
``` bash
export $(cat .env | xargs)
```


1. Preparing env ( this and next run in bash)
- Run
``` bash
export TF_VAR_ENV="dev"
git checkout origin/infra-${TF_VAR_ENV} -- versions.json
terraform init -backend-config="env/${TF_VAR_ENV}.conf"
terraform workspace select ${TF_VAR_ENV}
```

2. Terraform plan check
- Run
``` bash
terraform plan -var-file "env/${TF_VAR_ENV}.tfvars" -var="components_versions=$(cat versions.json)"
```

3. Terraform apply
- Run
``` bash
terraform apply -var-file "env/${TF_VAR_ENV}.tfvars" -var="components_versions=$(cat versions.json)"
```


<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.53.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ecs_task_definition_grafana"></a> [ecs\_task\_definition\_grafana](#module\_ecs\_task\_definition\_grafana) | ../../github/aws-ecs-task-definition | n/a |
| <a name="module_ecs_task_service_grafana"></a> [ecs\_task\_service\_grafana](#module\_ecs\_task\_service\_grafana) | ../../github/aws-ecs-task-service | n/a |

## Resources

| Name | Type |
|------|------|
| [terraform_remote_state.cicd_infra](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ENV"></a> [ENV](#input\_ENV) | defines the name of the environment(dev, prod, etc). Should be defined as env variable, for example export TF\_VAR\_ENV=dev | `string` | n/a | yes |
| <a name="input_components_versions"></a> [components\_versions](#input\_components\_versions) | n/a | <pre>object({<br>    laba-service-stack = string<br><br>  })</pre> | n/a | yes |
| <a name="input_environment_type"></a> [environment\_type](#input\_environment\_type) | n/a | `map` | <pre>{<br>  "dev": "DEVELOPMENT",<br>  "preprod": "PRE-PRODUCTION",<br>  "prod": "PRODUCTION",<br>  "sandbox": "SANDBOX"<br>}</pre> | no |
| <a name="input_grafana"></a> [grafana](#input\_grafana) | n/a | `map` | <pre>{<br>  "container_image": "grafana/grafana",<br>  "container_name": "grafana",<br>  "port_mappings": [<br>    {<br>      "containerPort": 80,<br>      "hostPort": 80,<br>      "protocol": "tcp"<br>    },<br>    {<br>      "containerPort": 3000,<br>      "hostPort": 3000,<br>      "protocol": "tcp"<br>    }<br>  ],<br>  "task_cpu": 256,<br>  "task_memory": 512<br>}</pre> | no |
| <a name="input_grafana_task_role_policy_statements"></a> [grafana\_task\_role\_policy\_statements](#input\_grafana\_task\_role\_policy\_statements) | n/a | `map` | `{}` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | n/a | `string` | `"mb"` | no |
| <a name="input_region"></a> [region](#input\_region) | ########################################################### region and vpc variables ############################################################ | `string` | `"us-east-1"` | no |
| <a name="input_stack"></a> [stack](#input\_stack) | n/a | `string` | `"laba"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->