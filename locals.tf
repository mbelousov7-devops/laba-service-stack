locals {

  environment_type = lookup(var.environment_type, var.ENV, var.environment_type.sandbox)

  default_tags = {
    "org:resource-owner"   = "mbelousov7@gmail.com"
    "org:environment-type" = lookup(var.environment_type, var.ENV, var.environment_type.sandbox)
  }

  labels = merge(
    { env = var.ENV },
    { prefix = var.prefix },
    { stack = var.stack }
  )

}