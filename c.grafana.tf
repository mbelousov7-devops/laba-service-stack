locals {
  grafana_labels = merge(
    local.labels,
    { component = "grafana" },
  )

  grafana_ingress_rules = [
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "Allow HTTPS access"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "Allow HTTP access"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  grafana_egress_rules = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow All access"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]


}

module "grafana_security_group" {
  source        = "git::https://github.com/mbelousov7/aws-security-group.git?ref=main"
  vpc_id        = local.vpc_id
  ingress_rules = local.grafana_ingress_rules
  egress_rules  = local.grafana_egress_rules
  labels        = local.grafana_labels
}


module "grafana_ecs_task_definition" {
  source = "git::https://github.com/mbelousov7/aws-ecs-task-definition.git?ref=main"
  //source                      = "../../github/aws-ecs-task-definition"
  aws_region                  = var.region
  container_name              = var.grafana.container_name
  container_image             = var.grafana.container_image
  task_cpu                    = var.grafana.task_cpu
  task_memory                 = var.grafana.task_memory
  port_mappings               = var.grafana.port_mappings
  task_role_policy_arns       = []
  task_role_policy_statements = var.grafana_task_role_policy_statements
  labels                      = local.grafana_labels
}

module "grafana_ecs_task_service" {
  source = "git::https://github.com/mbelousov7/aws-ecs-task-service.git?ref=main"
  //source              = "../../github/aws-ecs-task-service"
  count                   = local.subnet_id_public != "" ? 1 : 0
  task_definition_arn     = module.grafana_ecs_task_definition.task_definition_arn
  task_subnet_ids         = [local.subnet_id_public]
  task_security_group_ids = [module.grafana_security_group.id]
  assign_public_ip        = true
  labels                  = local.grafana_labels
}
