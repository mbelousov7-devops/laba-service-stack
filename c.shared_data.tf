locals {
  vpc_id                       = data.terraform_remote_state.cicd_infra.outputs.vpc_id
  subnet_id_public             = data.terraform_remote_state.cicd_infra.outputs.subnet_id_public
  subnet_id_private            = data.terraform_remote_state.cicd_infra.outputs.subnet_id_private
  subnet_id                    = local.subnet_id_private
  artifacts_ecr_repository_url = data.terraform_remote_state.cicd_infra.outputs.repository_url

}

data "terraform_remote_state" "cicd_infra" {
  backend = "s3"
  config = {
    encrypt = true
    bucket  = "mbelousov7-devops-terraform-state"
    key     = "cicd-infra-service-stack/terraform.tfstate"
    region  = "us-east-1"
  }
  workspace = var.account_name

}




