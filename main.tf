provider "aws" {
  region = var.region
  default_tags {
    tags = merge(local.default_tags, )
  }
}

terraform {
  backend "s3" {
    # see detailed bucket configuration in env/<ENV>.conf file
    encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.53.0"
    }
  }
}
