locals {
  laba_py_image_component_image = "${local.artifacts_ecr_repository_url}:${var.laba_py_image_component.container_name}-${var.components_versions.laba-py-image-component}"
  laba_py_image_component_labels = merge(
    local.labels,
    { component = "laba-py-image-component" },
  )

  laba_py_image_component_ingress_rules = [
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "Allow HTTPS access"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "Allow HTTP access"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "Allow HTTP access"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  laba_py_image_component_egress_rules = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow All access"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]


}

module "laba_py_image_component_security_group" {
  source        = "git::https://github.com/mbelousov7/aws-security-group.git?ref=main"
  vpc_id        = local.vpc_id
  ingress_rules = local.laba_py_image_component_ingress_rules
  egress_rules  = local.laba_py_image_component_egress_rules
  labels        = local.laba_py_image_component_labels
}


module "laba_py_image_component_ecs_task_definition" {
  source                      = "git::https://github.com/mbelousov7/aws-ecs-task-definition.git?ref=main"
  aws_region                  = var.region
  container_name              = var.laba_py_image_component.container_name
  container_image             = local.laba_py_image_component_image
  task_cpu                    = var.laba_py_image_component.task_cpu
  task_memory                 = var.laba_py_image_component.task_memory
  port_mappings               = var.laba_py_image_component.port_mappings
  task_role_policy_arns       = []
  task_role_policy_statements = var.laba_py_image_component_task_role_policy_statements
  labels                      = local.laba_py_image_component_labels
}

module "laba_py_image_component_ecs_task_service" {
  source                  = "git::https://github.com/mbelousov7/aws-ecs-task-service.git?ref=main"
  task_definition_arn     = module.laba_py_image_component_ecs_task_definition.task_definition_arn
  task_subnet_ids         = [local.subnet_id]
  task_security_group_ids = [module.laba_py_image_component_security_group.id]
  assign_public_ip        = true
  labels                  = local.laba_py_image_component_labels
}
