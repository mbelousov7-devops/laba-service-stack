############################################################ environment variables ############################################################
variable "ENV" {
  type        = string
  description = "defines the name of the environment(dev, prod, etc). Should be defined as env variable, for example export TF_VAR_ENV=dev"

  validation {
    condition     = contains(["dev", "preprod", "prod"], var.ENV)
    error_message = "Valid values for var are (dev, preprod, prod)."
  }
}

variable "account_name" {
  type        = string
  description = "defines the name of the aws environment(preprod, prod,)"

  validation {
    condition     = contains(["preprod", "prod"], var.account_name)
    error_message = "Valid values for var are (preprod, prod)."
  }
}

variable "environment_type" {
  default = {
    dev     = "DEVELOPMENT"
    preprod = "PRE-PRODUCTION"
    prod    = "PRODUCTION"
    sandbox = "SANDBOX"
  }
}

variable "prefix" {
  type    = string
  default = "mb"
}

variable "stack" {
  default = "laba"
}

variable "components_versions" {
  type = object({
    laba-service-stack      = string
    laba-py-image-component = string

  })
}

############################################################ region and vpc variables ############################################################
variable "region" {
  type    = string
  default = "us-east-1"
}

############################################################ grafana configs ############################################################

variable "grafana" {
  default = {
    container_name  = "grafana"
    container_image = "grafana/grafana"
    task_cpu        = 256
    task_memory     = 512
    port_mappings = [
      {
        containerPort = 80
        hostPort      = 80
        protocol      = "tcp"
      },
      {
        containerPort = 3000
        hostPort      = 3000
        protocol      = "tcp"
      }
    ]
  }
}

variable "grafana_task_role_policy_statements" {
  default = {}
}

############################################################ laba-py-image-component configs ############################################################

variable "laba_py_image_component" {
  default = {
    container_name = "laba-py-image-component"
    task_cpu       = 256
    task_memory    = 512
    port_mappings = [
      {
        containerPort = 80
        hostPort      = 80
        protocol      = "tcp"
      },
      {
        containerPort = 8080
        hostPort      = 8080
        protocol      = "tcp"
      }
    ]
  }
}

variable "laba_py_image_component_task_role_policy_statements" {
  default = {}
}

